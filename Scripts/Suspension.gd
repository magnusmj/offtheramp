extends CollisionShape

var wheel: Spatial
var hub: Spatial
var original_transform: Transform
var compression = 0
var compression_added = false

export var spring_constant = 8.0
export var damping = 1
export var limit = 1.2

func _ready():
	original_transform = transform
	original_transform.origin.y -= 3.0*limit/5.0
	wheel = $Hub/Wheel
	hub = $Hub
	
func set_compression(value):	
	compression = -value	
	compression = max(0.0,compression)
	transform.origin.y = original_transform.origin.y + min(limit,compression)

func get_force():	
	return spring_constant*max(0,compression) + max(0, compression-limit)*200
