extends Control

var input = STD.INPUT.NONE
var car = null

signal input_changed(input)

func _ready():
	pass

func set_car(value):
	car = value
	$ConfigMenu/RPMLimit.text = str(car.engine_speed_limit*1000)
	$ConfigMenu/MaxTorque.text = str(car.engine_torque_multiplier*20)
	$ConfigMenu/MaxRPM.text = str(car.engine_speed_max*1000)

func _on_accelerate_button_down():
	if not input & STD.INPUT.ACCELERATE:
		input += STD.INPUT.ACCELERATE
	emit_signal("input_changed", input)

func _on_accelerate_button_up():
	if input & STD.INPUT.ACCELERATE:
		input -= STD.INPUT.ACCELERATE
	emit_signal("input_changed", input)

func _on_gear_down_pressed():
	emit_signal("input_changed", input+STD.INPUT.GEARDOWN)

func _on_gear_up_pressed():
	emit_signal("input_changed", input+STD.INPUT.GEARUP)


func _input(event):
	if Input.is_action_just_pressed("gear_up"):
		emit_signal("input_changed", input+STD.INPUT.GEARUP)
	if Input.is_action_just_pressed("gear_down"):
		emit_signal("input_changed", input+STD.INPUT.GEARDOWN)
	if Input.is_action_just_pressed("left") and not input & STD.INPUT.LEFT:
		input += STD.INPUT.LEFT
		emit_signal("input_changed", input)
	if Input.is_action_just_released("left") and input & STD.INPUT.LEFT:
		input -= STD.INPUT.LEFT
		emit_signal("input_changed", input)	
	if Input.is_action_just_pressed("right") and not input & STD.INPUT.RIGHT:
		input += STD.INPUT.RIGHT
		emit_signal("input_changed", input)
	if Input.is_action_just_released("right") and input & STD.INPUT.RIGHT:
		input -= STD.INPUT.RIGHT
		emit_signal("input_changed", input)	
	if Input.is_action_just_pressed("accelerate") and not input & STD.INPUT.ACCELERATE:
		input += STD.INPUT.ACCELERATE
		emit_signal("input_changed", input)
	if Input.is_action_just_released("accelerate") and input & STD.INPUT.ACCELERATE:
		input -= STD.INPUT.ACCELERATE
		emit_signal("input_changed", input)
	if Input.is_action_just_pressed("brake") and not input & STD.INPUT.BRAKE:
		input += STD.INPUT.BRAKE
		emit_signal("input_changed", input)
	if Input.is_action_just_released("brake") and input & STD.INPUT.BRAKE:
		input -= STD.INPUT.BRAKE
		emit_signal("input_changed", input)

func _on_brake_button_down():
	if not input & STD.INPUT.BRAKE:
		input += STD.INPUT.BRAKE
	emit_signal("input_changed", input)

func _on_brake_button_up():
	if input & STD.INPUT.BRAKE:
		input -= STD.INPUT.BRAKE
	emit_signal("input_changed", input)

func _on_reset_pressed():
	get_tree().reload_current_scene()


func _on_config_pressed():
	$ConfigMenu.visible = not $ConfigMenu.visible


func _on_rpm_limit_text_entered(new_text):
	if new_text == "false":
		$ConfigMenu/RPMLimit.text = str(car.engine_speed_limit*1000)
		return
	var limit = float(new_text)
	limit = max(3000, limit)
	limit = min(20000, limit)
	car.engine_speed_limit = limit / 1000.0


func _on_max_rpm_text_entered(new_text):
	if new_text == "false":
		$ConfigMenu/MaxRPM.text = str(car.engine_speed_max*1000)
		return
	var limit = float(new_text)
	limit = max(3000, limit)
	limit = min(20000, limit)
	car.engine_speed_max = limit / 1000.0


func _on_max_torque_text_entered(new_text):
	if new_text == "false":
		$ConfigMenu/MaxTorque.text = str(car.engine_torque_multiplier*20)
		return
	var limit = float(new_text)
	limit = max(20, limit)
	limit = min(500, limit)
	car.engine_torque_multiplier = limit / 20.0
