extends KinematicBody

const max_wheel_turn = 35
const raycast_offset = 3.0
var input = STD.INPUT.NONE 
var gravity = Vector3(0,-18.82,0)
 
var ground_normal = Vector3(0,1,0)

var wheel_rotation = 0
var rear_wheel_grip = false
var front_wheel_grip = false

#var car_speed = 0 # m/s
var car_weight = 1000 # kg
var car_velocity: Vector3
var car_acceleration: Vector3
var car_momentum: Vector3

var gears = [-0.3, 0.0, 0.33, 0.5, 0.7, 1.0, 1.2, 1.4]
var gear_index = 1 

export(Curve) var engine_torque
var engine_torque_multiplier = 5.0
var engine_load = 1.0
var engine_speed_idle = 0.7
var engine_speed_max = 7.0
var engine_speed = 0.7
var engine_speed_limit = 6.0
var engine_limited = 0.0

var clutch_torque_max = 1000 # N*m

var brake_strength = 1*10e5

var engine_audio_max: AudioStreamPlayer3D = null
var engine_audio_idle: AudioStreamPlayer3D = null

var suspension_front_left = null
var suspension_front_right = null
var suspension_rear_left = null
var suspension_rear_right = null
var ray_cast_front_left: RayCast
var ray_cast_front_right: RayCast
var ray_cast_rear_left: RayCast
var ray_cast_rear_right: RayCast

func _ready():
	engine_audio_max = $AudioEngineMax
	engine_audio_idle = $AudioEngineIdle
	suspension_front_left = $SuspensionFL
	suspension_front_right = $SuspensionFR
	suspension_rear_left = $SuspensionRL
	suspension_rear_right = $SuspensionRR
	ray_cast_front_left = $RayCastFL
	ray_cast_front_right = $RayCastFR
	ray_cast_rear_left = $RayCastRL
	ray_cast_rear_right = $RayCastRR
	var hud = $HUD
	if hud != null:
		hud.set_car(self)

func _physics_process(delta):
	process_suspension(delta)

func _process(delta):		
	var acc_forward = acceleration_forward()
	var car_speed = process_engine(delta, acc_forward)
	process_wheels(delta, acc_forward, car_speed)
	var forward = transform.basis.x
	var right = transform.basis.z
	var time_delta = delta/0.016
	transform.basis = transform.basis.rotated(forward, car_momentum.x*time_delta)
	transform.basis = transform.basis.rotated(right, car_momentum.z*time_delta)
	
func set_input(new_input):	
	input = new_input
	var gear_changed = false
	if input & STD.INPUT.GEARUP and gear_index < gears.size()-1:
		gear_index += 1
		gear_changed = true		
	if input & STD.INPUT.GEARDOWN and gear_index > 0:
		gear_index -= 1
		gear_changed = true	
	if gear_changed:
		pass
		#if gears[gear_index] != 0:
		#	engine_speed = car_speed/(gears[gear_index]*20)
	
func process_engine(delta, acc_forward):	
	var current_max_torque = engine_torque.interpolate(engine_speed/engine_speed_max)	
	var car_speed = forward_speed(acc_forward)
	var car_speed_delta = 0
	var drag = -car_velocity.normalized()*pow(car_speed, 2)*0.0003
	
	if (input & STD.INPUT.ACCELERATE) and engine_limited <= 0:
		engine_load += delta*20*current_max_torque
		engine_load = min(engine_load, 1)		
	else:
		engine_load -= delta*10
		engine_load = max(engine_load, 0)
	var engine_speed_delta = 0
	if engine_load > 0.1:
		
		engine_speed_delta = current_max_torque*engine_torque_multiplier*engine_load*delta
		
		if rear_wheel_grip and gears[gear_index] != 0:
			var gear_multiplier = 1/abs(pow(4*gears[gear_index],2))
			car_speed_delta = engine_speed_delta*gears[gear_index]*20*gear_multiplier
			engine_speed_delta *= gear_multiplier
			
			var target_engine_speed = car_speed/(gears[gear_index]*20)
			engine_speed = lerp (engine_speed, target_engine_speed, 0.1)
			car_speed_delta += (engine_speed-target_engine_speed)*0.02/gears[gear_index]
			car_momentum.z += car_speed_delta/200
		
		engine_speed += engine_speed_delta
		engine_speed = min(engine_speed_max, engine_speed)
	else:
		if rear_wheel_grip and gears[gear_index] != 0:
			var gear_multiplier = 1/abs(pow(4*gears[gear_index],2))
			engine_speed_delta = -delta*2*gear_multiplier
			engine_speed += engine_speed_delta
			car_momentum.z -= 0.002*gear_multiplier
			car_speed_delta = engine_speed_delta*gears[gear_index]*20*gear_multiplier
			var target_engine_speed = car_speed/(gears[gear_index]*20)
			engine_speed = lerp (engine_speed, target_engine_speed, 0.2)
		else:
			engine_speed -= delta*engine_torque_multiplier/2
		engine_speed = max(engine_speed_idle, engine_speed)
	
	if engine_speed >= engine_speed_limit:
		engine_limited += 0.004
	else:
		engine_limited -= delta
		engine_limited = max(0, engine_limited)
	
	car_velocity += acc_forward*(car_speed_delta) + drag*delta
	
	engine_audio_max.pitch_scale = engine_speed*0.5
	engine_audio_idle.pitch_scale = engine_speed*1*1.3
	
	engine_audio_max.unit_db = -15 + engine_load*current_max_torque * 18
	engine_audio_idle.unit_db = 3 - engine_load*current_max_torque * 6
	
	return car_speed

func acceleration_forward():
	var forward = transform.basis.x
	var norm_dist = forward.dot(ground_normal)
	forward -= norm_dist*ground_normal
	forward = forward.normalized()	
	return forward

func forward_speed(forward):
	var forward_speed = car_velocity.dot(forward)
	return forward_speed
	
func process_wheels(delta, acc_forward, car_speed):
	var acc = Input.get_accelerometer()
	var gyr = Input.get_gyroscope() 
	wheel_rotation += gyr.z
	if (abs(acc.x) < 0.5):
		 wheel_rotation  = lerp(wheel_rotation, 0, 0.1)
	if input & STD.INPUT.LEFT:
		 wheel_rotation += delta*200/max(1, car_speed/20)
	if input & STD.INPUT.RIGHT:
		 wheel_rotation -= delta*200/max(1, car_speed/20)

	wheel_rotation = min(max_wheel_turn, max(-max_wheel_turn, wheel_rotation))
	var rot = Basis().rotated(Vector3(0,0,1), deg2rad(wheel_rotation))
	suspension_front_left.hub.transform.basis = rot
	suspension_front_right.hub.transform.basis = rot
	
	suspension_front_left.wheel.transform.basis = suspension_front_left.wheel.transform.basis.rotated(Vector3(0,1,0), deg2rad(car_speed)*0.5)
	suspension_front_right.wheel.transform.basis = suspension_front_right.wheel.transform.basis.rotated(Vector3(0,1,0), deg2rad(car_speed)*0.5)
	
	
	if (input & STD.INPUT.BRAKE and car_speed != 0 and front_wheel_grip):
		var brake = min(1, max(0,1/pow(car_speed,2)))
		var brake_delta = brake*delta*brake_strength
		brake_delta = min(1.0, brake_delta)
		brake_delta = min(abs(car_speed), brake_delta)
		if car_speed<0:
			car_velocity += acc_forward*brake_delta
		else:
			car_velocity -= acc_forward*brake_delta
	
	if front_wheel_grip:
		var forward = transform.basis.x.normalized()
		var right = transform.basis.z.normalized()
		var right_speed = right.dot(car_velocity)/right.length()
		var forward_speed = forward.dot(car_velocity)/forward.length()
		
		var norm_dist = forward.dot(ground_normal)
		forward -= norm_dist*ground_normal
		forward = forward.normalized()
		car_momentum.x += forward_speed*wheel_rotation*delta*0.0005
		car_velocity -= forward*forward_speed
		car_velocity -= right*right_speed
		transform.basis = transform.basis.rotated(Vector3(0,1,0), deg2rad(car_speed*wheel_rotation)*0.0025)
		forward = transform.basis.x.normalized()
		norm_dist = forward.dot(ground_normal)
		forward -= norm_dist*ground_normal
		forward = forward.normalized()
		car_velocity += forward*forward_speed

func process_suspension(delta):	
	var up = transform.basis.y
	var forward = transform.basis.x
	var right = transform.basis.z
	
	car_velocity += car_acceleration*delta
	car_velocity = move_and_slide(car_velocity)
	var slides = get_slide_count()	
	
	if ray_cast_front_left.is_colliding():
		ground_normal += ray_cast_front_left.get_collision_normal()
		var pos = ray_cast_front_left.get_collision_point()
		var rpos = ray_cast_front_left.get_global_transform().origin
		var dist = (rpos - pos).dot(up) - raycast_offset
		suspension_front_left.set_compression(dist)
	else:
		suspension_front_left.set_compression(0)
	
	if ray_cast_front_right.is_colliding():
		ground_normal += ray_cast_front_right.get_collision_normal()
		var pos = ray_cast_front_right.get_collision_point()
		var rpos = ray_cast_front_right.get_global_transform().origin
		var dist = (rpos - pos).dot(up) - raycast_offset
		suspension_front_right.set_compression(dist)
	else:
		suspension_front_right.set_compression(0)
		
	if ray_cast_rear_left.is_colliding():
		ground_normal += ray_cast_rear_left.get_collision_normal()
		var pos = ray_cast_rear_left.get_collision_point()		
		var rpos = ray_cast_rear_left.get_global_transform().origin
		var dist = (rpos - pos).dot(up) - raycast_offset
		suspension_rear_left.set_compression(dist)
	else:
		suspension_rear_left.set_compression(0)
	
	if ray_cast_rear_right.is_colliding():
		ground_normal += ray_cast_rear_right.get_collision_normal()
		var pos = ray_cast_rear_right.get_collision_point()		
		var rpos = ray_cast_rear_right.get_global_transform().origin
		var dist = (rpos - pos).dot(up) - raycast_offset
		suspension_rear_right.set_compression(dist)
	else:
		suspension_rear_right.set_compression(0)

	ground_normal = ground_normal.normalized()
	
	car_acceleration = gravity
	if up.length() == 0:
		return
	var upspeed = car_velocity.dot(ground_normal)
	var time_delta = delta/0.016
	if suspension_front_left.compression > 0:
		handle_suspender(time_delta, suspension_front_left, upspeed, up, 3.5, 4.0)
	if suspension_front_right.compression > 0:
		handle_suspender(time_delta, suspension_front_right, upspeed, up, -3.5, 4.0)
	if suspension_rear_left.compression > 0:
		handle_suspender(time_delta, suspension_rear_left, upspeed, up, 3.5, -4.0)
	if suspension_rear_right.compression > 0:
		handle_suspender(time_delta, suspension_rear_right, upspeed, up, -3.5, -4.0)	
	
	if suspension_rear_left.compression > 0 or suspension_rear_right.compression > 0:
		rear_wheel_grip = true
	else:
		rear_wheel_grip = false
	if suspension_front_left.compression > 0 or suspension_front_right.compression > 0:
		front_wheel_grip = true
	else:
		front_wheel_grip = false

func handle_suspender(delta, suspension, upspeed, up, xpos, zpos):
	var rot_factor = 0.0016
	var momentum_damp = 0.96 
	var mom_x_speed = car_momentum.x*xpos*PI 
	var mom_z_speed = car_momentum.z*zpos*PI 
	var speed = upspeed+mom_x_speed+mom_z_speed
	var up_damp = 1 + max(0, speed*suspension.damping)
	var down_damp = 1 + max(0, -speed*suspension.damping)
	var damp = down_damp/up_damp
	var force = suspension.get_force()*up*damp
	car_acceleration += force*delta
	var forcel = min(force.length(), suspension.spring_constant*suspension.limit)
	car_momentum.x += forcel*rot_factor*delta/xpos
	car_momentum.z += forcel*rot_factor*delta/zpos
	car_momentum *= momentum_damp